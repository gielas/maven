package com.example.jdbcdemo.domain;

public class Address {
	
	private long id;
	private String streetName;
	private String cityName;
	private String zipCode;
	private String countryName;
	
	public Address() {
	}
	
	public Address(String streetName, String cityName, String zipCode, String countryName) {
		super();
		this.streetName = streetName;
		this.cityName = cityName;
		this.zipCode = zipCode;
		this.countryName = countryName;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
}

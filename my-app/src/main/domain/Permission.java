package com.example.jdbcdemo.domain;

public class Permission {
	
	private long id;
	private int permissionLevel;
	
	public Permission() {
	}
	
	public Permission(int permissionLevel) {
		super();
		this.permissionLevel = permissionLevel;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	
	public int getPermissionLevel() {
		return permissionLevel;
	}
	public void setPermissionLevel(int permissionLevel) {
		this.permissionLevel = permissionLevel;
	}
}

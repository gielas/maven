package com.example.jdbcdemo.domain;

public class Person {
	
	private long id;
	private String firstName;
	private String lastName;
	private String[] phoneNumber;
	private Address[] address;
	private Role[] role;
	private Permission[] permision;
	
	public Person(){
	}
	
	public Person(String firstName, String lastName, String[] phoneNumber, Address[] address, Role[] role, Permission[] permision) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.role = role;
		this.permision = permision;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String[] getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String[] phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Address[] getAddress() {
		return address;
	}
	public void setAddress(Address[] address) {
		this.address = address;
	}
	public Role[] getRole() {
		return role;
	}
	public void setRole(Role[] role) {
		this.role = role;
	}
	public Permission[] getPermision() {
		return permision;
	}
	public void setPermision(Permission[] permision) {
		this.permision = permision;
	}
}

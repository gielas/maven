package com.example.jdbcdemo.domain;

public class Role {
	
	private long id;
	private Permission roleType;
	
	public Role(){
	}
	public Role(Permission roleType) {
		super();
		this.roleType = roleType;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;

	public Permission getRoleType() {
		return roleType;
	}
	public void setRoleType(Permission roleType) {
		this.roleType = roleType;
	}	
}

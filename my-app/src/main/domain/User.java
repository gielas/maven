package com.example.jdbcdemo.domain;

public class User {
	
	private long id;
	private String login;
	private String password;
	private Person person;
	
	public User(){
	}
	
	public User(String login, String password, Person person) {
		super();
		this.login = login;
		this.password = password;
		this.person = person;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
}

package com.example.jdbcdemo.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.example.jdbcdemo.domain.Address;
import com.example.jdbcdemo.service.AddressManager;

public class AddressManagerTest {
	
	AddressManager addressManager = new AddressManager();
	
	private final static String STREETNAME_1 = "Brzegi";
	private final static String CITYNAME_1 = "Gdansk";
	private final static String ZIPCODE_1 = "12-345";
	private final static String COUNTRYNAME_1 = "Polska";
	
	@Test
	public void checkConnection(){
		assertNotNull(addressManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Address address = new Address(STREETNAME_1, CITYNAME_1, ZIPCODE_1, COUNTRYNAME_1);
		
		addressManager.clearAddress();
		assertEquals(1,addressManager.addAddress(address));
		
		List<Address> address = addressManager.getAllAddress();
		Address addressRetrieved = address.get(0);
		
		assertEquals(STREETNAME_1, addressRetrieved.getStreetName());
		assertEquals(CITYNAME_1, addressRetrieved.getCityName());
		assertEquals(ZIPCODE_1, addressRetrieved.getZipCode());
		assertEquals(COUNTRYNAME_1, addressRetrieved.getCountryName());
		
	}

}
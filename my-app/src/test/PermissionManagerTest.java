package com.example.jdbcdemo.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.example.jdbcdemo.domain.Permission;
import com.example.jdbcdemo.service.PermissionManager;

public class PermissionManagerTest {
	
	PermissionManager permissionManager = new PermissionManager();
	
	private final static int PERMISSIONLEVEL_1 = 1;
	
	@Test
	public void checkConnection(){
		assertNotNull(permissionManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Permission permission = new Permission(PERMISSIONTYPE_1);
		
		permissionManager.clearPermission();
		assertEquals(1,permissionManager.addPermission(permission));
		
		List<Permission> permission = permissionManager.getAllPermission();
		Permission permissionRetrieved = permission.get(0);
		
		assertEquals(PERMISSIONTYPE_1, permissionRetrieved.getPermissionLevel());
		
	}

}
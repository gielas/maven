package com.example.jdbcdemo.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.example.jdbcdemo.domain.Person;
import com.example.jdbcdemo.service.PersonManager;

public class PersonManagerTest {

	PersonManager personManager = new PersonManager();
	
	private final static String FIRSTNAME_1 = "Damian";
	private final static String LASTNAME_1 = "Gielazyn";
	private final static String PHONENUMBER_1 = "123456789";
	private final static String ADDRESS_1 = "Brzegi 55";
	private final static int ROLE_1 = "1";
	private final static int PERMISSION_1 = "1";
	
	@Test
	public void checkConnection(){
		assertNotNull(personManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Person person = new Person(FIRSTNAME_1, LASTNAME_1, PHONENUMBER_1, ADDRESS_1, ROLE_1, PERMISSION_1);
		
		personManager.clearPerson();
		assertEquals(1,personManager.addPerson(person));
		
		List<Person> person = personManager.getAllPerson();
		Person personRetrieved = person.get(0);
		
		assertEquals(FIRSTNAME_1, personRetrieved.getFirstName());
		assertEquals(LASTNAME_1, personRetrieved.getLastName());
		assertEquals(PHONENUMBER_1, personRetrieved.getPhoneNumber());
		assertEquals(ADDRESS_1, personRetrieved.getAddress());
		assertEquals(ROLE_1, personRetrieved.getRole());
		assertEquals(PERMISSION_1, personRetrieved.getPermision());
		
	}

}
package com.example.jdbcdemo.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.example.jdbcdemo.domain.Role;
import com.example.jdbcdemo.service.RoleManager;

public class RoleManagerTest {
	
	RoleManager roleManager = new RoleManager();
	
	private final static Permission ROLETYPE_1 = 1;
	
	@Test
	public void checkConnection(){
		assertNotNull(permissionManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Role role = new Role(ROLETYPE_1);
		
		permissionManager.clearPermission();
		assertEquals(1,permissionManager.addRole(role));
		
		List<Role> role = roleManager.getAllRole();
		Role roleRetrieved = role.get(0);
		
		assertEquals(ROLETYPE_1, roleRetrieved.getRoleType());
		
	}

}
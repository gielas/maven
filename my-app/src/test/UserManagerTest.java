package com.example.jdbcdemo.test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.example.jdbcdemo.domain.User;
import com.example.jdbcdemo.service.UserManager;

public class UserManagerTest {
	
	UserManager userManager = new UserManager();
	
	private final static String LOGIN_1 = "login";
	private final static String PASSWORD_1 = "password#";	
	
	@Test
	public void checkConnection(){
		assertNotNull(userManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		User user = new User(LOGIN_1, PASSWORD_1, null);
		
		userManager.clearUser();
		assertEquals(1,userManager.addUser(user));
		
		List<User> user = userManager.getAllUser();
		User userRetrieved = user.get(0);
		
		assertEquals(LOGIN_1, userRetrieved.getLogin());
		assertEquals(PASSWORD_1, userRetrieved.getPassword());
		
	}

}